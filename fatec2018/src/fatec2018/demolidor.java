package fatec2018;

import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.*;
import robocode.AdvancedRobot;

public class demolidor extends AdvancedRobot {

    int sentidoRotacao = 1; // Define se o robo vai girar no sentido Horario oU Anti Horario

    public void run() { // tudo o que estiver dentro deste parametro vai rodar a partir do start

// Definicoes de Cores
        setBodyColor(new Color(170, 18, 18));
        setGunColor(new Color(255, 208, 40));
        setRadarColor(new Color(201, 167, 44));
        setScanColor(new Color(178, 41, 61));
        setBulletColor(new Color(255, 255, 37));

        while (true) { //Em quanto a condição for verdadeira, vai executar o código a baixo:
            turnRight(12 * sentidoRotacao);
            // Girar o tank em 5 multiplicado pela rotação, o que vai dar a INIMIGOação...
            // mais curta para girar            
        }
    }

    public void onScannedRobot(ScannedRobotEvent INIMIGO) {

        double EnergiaInimigo = getEnergy(); //pega a energia do robô ADVERSÀRIO

        String nomeRobo = INIMIGO.getName();  //captura o nome do robo detectado

        double cooX = getX(); //pega a coordenada X do MEU robô
        double cooY = getY(); //pega a coordenada Y do MEU robô     

        double hdg = getHeading(); //Captura a proa do tank

        String aliado1 = "samplesentry.BorderGuard"; //informar o nome do aliado 1
        String aliado2 = "fatec2018.demolidor* (1)"; //informar o nome do aliado 2
        String aliado3 = "fatec2018.demolidor* (2)"; //informar o nome do aliado 2
        String aliado4 = "fatec2018.demolidor* (3)"; //informar o nome do aliado 2

        if (nomeRobo.equals(aliado1) || nomeRobo.equals(aliado2) || nomeRobo.equals(aliado3) || nomeRobo.equals(aliado4)) { //caso o nome do robo detectado seja igual ao dos robôs aliados ele não vai disparar
            setBodyColor(new Color(0, 200, 0));
        } else {

            //caso o nome do robo detectado seja igual ao dos robôs aliados ele não vai disparar
            setBodyColor(new Color(200, 18, 18));
            setBodyColor(new Color(220, 18, 18));
            setBodyColor(new Color(240, 18, 18));
            setBodyColor(new Color(250, 18, 18));

            //while (situacao == 0) {
            // o "INIMIGO" corresponde ao ângulo relativo entre o robô e o inimigo
            if (INIMIGO.getBearing() >= 0) {
                sentidoRotacao = 1;
            } else {
                sentidoRotacao = -1;
            }
            if (INIMIGO.getVelocity() < 1) {
                fire(1);
            }
            setAhead(INIMIGO.getDistance() - 80);
            //setTurnRight(e.getBearing());
            //Segue em frente até a posição do inimigo MENOS 80 pixel

            //setTurnRight(e.getBearing() - 10); //ir fazendo zig-zag [DESATIVADO]
            
            if (INIMIGO.getDistance()
                    <= 200) {  // Se o tank estiver a 200 ou menos do inimigo ira rodar...
                if (INIMIGO.getEnergy() > 20) {
                    fire(3); //se a energia for maior que 16, tiro 3
                } else if (INIMIGO.getEnergy() > 10) {
                    fire(2); //se a energia for maior que 16, tiro 2
                } else if (INIMIGO.getEnergy() > 4) {
                    fire(1); //se a energia for maior que 16, tiro 1
                } else if (INIMIGO.getEnergy() > 2) {
                    fire(.5); //se a energia for maior que 16, tiro 0.5
                } else if (INIMIGO.getEnergy() > .4) {
                    fire(.1); //se a energia for maior que 16, tiro 0.1
                }
                //scan();
            }
        }
    }

    public void onHitRobot(HitRobotEvent INIMIGO) {

        setBack(50);
        if (INIMIGO.getBearing() >= 0) {
            sentidoRotacao = 1;
        } else {
            sentidoRotacao = -1;
        }
        turnRight(INIMIGO.getBearing());

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fatec2018;

/**
 *
 * @author Maicom
 */
import robocode.*;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
/**
 * RoboA - a robot by (your name here)
 */
public class roboBase extends AdvancedRobot {

    /**
     * run: RoboA's default behavior
     */
    public void run() {
        // Initialization of the robot should be put here

        // After trying out your robot, try uncommenting the import at the top,
        // and the next line:
        // setColors(Color.red,Color.blue,Color.green); // body,gun,radar
        // Robot main loop
        double cX = getX();  // puxa a coordenada X
        double cY = getY();	 // puxa a coordenada Y
        turnGunLeft(90);
        while (true) {

            execute();
            setAhead(100);
            setTurnGunLeft(1);
            setTurnGunRight(2);

        }
    }

    /**
     * onScannedRobot: What to do when you see another robot
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        // Replace the next line with any behavior you would like

        String aliado1 = "fatec2018.roboBase* (1)"; //informar o nome do aliado 1
        String aliado2 = "fatec2018.roboBase* (2)"; //informar o nome do aliado 2

        String nomeRobo = getName();  //captura o nome do robo detectado

        if (nomeRobo.equals(aliado1) || nomeRobo.equals(aliado2)) { //caso o nome do robo detectado seja igual ao dos robôs aliados ele não vai disparar
           // setTurnRight(90);
        } else {
            fire(1);
        }

    }

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    public void onHitByBullet(HitByBulletEvent e) {
        // Replace the next line with any behavior you would like
        back(50);
    }

    /**
     * onHitWall: What to do when you hit a wall
     */
    public void onHitWall(HitWallEvent e) {
        // Replace the next line with any behavior you would like
        back(30);
        turnRight(180);
    }
}
